from osv import osv, fields

class salesman(osv.osv):

   _name="sale.salesmam"
   _inherit = "sale.salesmam"
   _columns={

             'name':fields.char('nombre',size=120,required=True),
             'duraction':fields.float('Duracion',required=True,digits=(14,3),help="Duracion"), 
             'description':fields.text('Descripcion',required=True),
             'goals':fields.text('Objetivos'),
             'notes':fields.text('Observaciones'),
             'active':fields.boolean('Activo'),
             }
salesmam()           